window.addEventListener("DOMContentLoaded", () => {
    // Join a session via ENTER
    // document = DOM, querySelector() = wählt ein Element im DOM aus [man kann es ansprechen]
    // addEventListener = man fügt einen Listener der auf bestimmte Events/Ereignisse achtet
    // keydown == wenn man auf eine Taste drückt
    // e wird mitgegeben damit man den keyCode noch bestimmen kann
    // value = die Zahlen die der Benutzer eingegeben hat
    // wenn keyCode == 13 (ENTER-Taste) ist dann rufe die Funktion auf
    document.querySelector("#sessionID").addEventListener("keyup", e => {
        const item = document.querySelector("#sessionID");
        isValid(item.value) ? item.classList.add("correct") : item.classList.remove("correct");
        if (e.keyCode == 13) visitSession(item.value);
    });

    // Join a session via Join-Button
    // document = DOM, querySelector() = wählt ein Element im DOM aus [man kann es ansprechen]
    // addEventListener = man fügt einen Listener der auf bestimmte Events/Ereignisse achtet
    // "click", wenn man auf das angesprochene Element clickt
    // rufe dann die Funktion visitionSession auf mit den Zahlen die der Benutzer eingegeben hat
    document.querySelector("#join").addEventListener("click", () => {
        visitSession(document.querySelector("#sessionID").value);
    });

    // Eine Funktion die den Input aufnimmt und testet ob es valide ist.
    // ist es valide? so führe window.open auf mit der sessionID
    // _blank = neues Tab erstellen und da den Link öffnen
    const visitSession = (sessionID = "") => {
        if (isValid(sessionID)) window.open("https://frag.jetzt/participant/room/" + sessionID, "_blank");
    }

    // Eine Funktionen die testet ob die angegeben Zahlen überhaupt Sinn ergeben
    // Es wird getestet auf: 
    // - leere Eingaben
    // - die Länge der Zahlen (frag.jetzt SessionID darf nur minimal und maximal 8 Zeichen enthalten)
    // - auf die Richtigkeit der Zahlen (ob sie auch nur ZAHLEN sind)
    const isValid = sessionID => {
        if (sessionID == "" || sessionID.length != 8) return false;
        return !isNaN(sessionID);
    }

    //////////////

    // Review Section

    // Einfache Arrays die die Elemente speichern
    // const = Variable als Konstante ("unveränderlich")
    // document = DOM
    // querySelectorAll = gibt all Elemente die dem Kriterium entsprechen in einem Array zurück
    const icon = document.querySelectorAll(".person");
    const opinion = document.querySelectorAll(".opinion");
    const people = document.querySelectorAll(".info-container");

    // Für jedes Element: fügt einen EventListener hinzu der auf "clicks" reagiert
    // Falls man darauf drücken sollte so starte die for-Schleife
    // die for-Schleife geht durch all Elemente im verschiedenen Array durch und spricht deren classList an
    // classList = die ganzen HTML Klassen
    // classList.remove("active") = entferne die Klasse "active" von den Elementen

    // it.classList.add("active") = füge für it (ein item/Element im icon Array) die Klasse "active" hinzu
    // für das opionion- und people-Array werden auch die Klassen "active" hinzgefügt
    // it.dataset.index spricht einfach nur auf die in HTML angegeben datasets ein
    icon.forEach(it => it.addEventListener("click", () => {
        for (let i = 0; i < icon.length; i++) {
            icon[i].classList.remove("active");
            opinion[i].classList.remove("active");
            people[i].classList.remove("active");
        }

        it.classList.add("active");
        opinion[it.dataset.index].classList.add("active");
        people[it.dataset.index].classList.add("active");
    }));
});